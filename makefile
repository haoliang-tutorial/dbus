.PHONY: install tests

freeze_as_requirements:
	@ poetry export --format requirements.txt --without-hashes > requirements.txt

install:
	@ python3 -m venv venv
	@ venv/bin/pip install -r requirements.txt


install_for_user: requirements.txt
	@ python3 -m pip install --user -r requirements.txt


shell:
	PYTHONPATH=${PWD} poetry shell


tests:
	@ poetry run pytest --color=yes --show-capture=all
