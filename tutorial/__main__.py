import trio

from tutorial import cli


async def main():

    # parsed = low_level.parse_signature(list("(susssasa{sv}i)"))
    # print("parsed", parsed)
    # print("len", len(parsed.fields))
    await cli.try_to_send_notification()


if __name__ == "__main__":
    trio.run(main)
