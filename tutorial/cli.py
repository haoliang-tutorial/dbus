import time

from jeepney import DBusAddress, MessageType, new_method_call
from jeepney.io.trio import (
    DBusConnection,
    DBusRouter,
    open_dbus_connection,
    open_dbus_router,
)


async def try_to_connect():
    conn: DBusConnection = await open_dbus_connection(bus="SESSION")
    async with conn:
        print("conn uniq-name", conn.unique_name)
        # await trio.sleep_forever()


async def try_to_ping():
    peer = DBusAddress(
        bus_name="org.freedesktop.DBus",
        object_path="/org/freedesktop/DBus",
        interface="org.freedesktop.DBus.Peer",
    )

    ping = new_method_call(peer, "Ping")

    router: DBusRouter
    async with open_dbus_router(bus="SESSION") as router:
        reply = await router.send_and_get_reply(ping)

    assert reply.header.message_type == MessageType.method_return
    assert reply.body == ()
    print(f"{reply!r}")


async def try_to_set_rime_ascii_mode():
    peer = DBusAddress(
        bus_name="org.fcitx.Fcitx5",
        object_path="/rime",
        interface="org.fcitx.Fcitx.Rime1",
    )

    router: DBusRouter
    async with open_dbus_router(bus="SESSION") as router:

        # call = new_method_call(peer, "GetCurrentSchema")
        # reply = await router.send_and_get_reply(call)
        # print("current schema", reply.body)

        # call = new_method_call(peer, "IsAsciiMode")
        # reply = await router.send_and_get_reply(call)
        # print("is ascii mode", reply.body)

        # call = new_method_call(peer, "ListAllSchemas")
        # reply = await router.send_and_get_reply(call)
        # print("schemas", reply.body)

        call = new_method_call(peer, "SetAsciiMode", signature="b", body=(True,))
        reply = await router.send_and_get_reply(call)
        print("set ascii mode", reply)


async def try_to_send_notification():
    peer = DBusAddress(
        bus_name="org.freedesktop.Notifications",
        object_path="/org/freedesktop/Notifications",
        interface="org.freedesktop.Notifications",
    )

    router: DBusRouter
    async with open_dbus_router(bus="SESSION") as router:

        call = new_method_call(
            peer,
            "Notify",
            signature="susssasa{sv}i",
            body=("myapp", 0, "", "mytitle", "mybody", [], [], -1),
        )
        start = time.time()
        reply = await router.send_and_get_reply(call)
        end = time.time()
        print("notify", reply)
        print("elapsed {:.4f} seconds".format(end - start))
